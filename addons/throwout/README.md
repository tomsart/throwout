
ThrowOut
========

Version 1.0, for Godot 4.2.

Scatter lots of instances from a MeshLibrary using physics in the editor.
Your MeshLibrary must have both mesh and colliders defined.


Using
-----

![Screenshot](screenshots/screenshot.png)

Copy `addons/throwout` to your project, and activate 
in `Project -> Project Settings -> Plugins`

Now in a scene, add a ThrowOut node. With that node selected,
there are some settings in the Inspector options for this node, and a
separate menu just above the viewport for some actions you can do,
once you have spawned some objects.

You can convert spawned RigidBody3D objects to MeshIntance3D,
MultiMeshInstance3D, or StaticBody3D instances, via that menu.

If physics is going nuts, or you want it to go nuts, you can turn it
on and off via that menu.

Change brush size by holding down control and dragging with the mouse.
The ring will turn yellow while you are changing size. Alternately,
you can change brush size and drop height in the Inspector options
for the ThrowOut node.


Demo
----
The demo scene at `addons/throwout/demo/throw_out_demo.tscn` has a node
with a sample MeshLibrary all set up. Open that after installing the addon,
drag on something, and stuff will drop!


TODO
----
- Undo/Redo
- Directional spout mode

