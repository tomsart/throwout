@icon("res://addons/throwout/icon.svg")
@tool
extends Node3D
class_name ThrowOut


# This object is parent to spawned rigid bodies that drop down in the editor.
# Requires the physics engine to be active in the editor, so beware.


@export_group("Settings")

## Pool of meshes to randomly spawn from
@export var meshes : MeshLibrary

## Diameter of spawn area
@export var spawn_radius := 3.0

## Meters above mouse point to spawn from
@export var spawn_height := 5.0

## Range (x..y) to randomize scale of spawned objects
@export var spawn_scale := Vector2(1.0, 1.0)

#@export_range(0.0, 1.0) var rotation_randomness := 1.0

## Minimum Y height of objects, below which spawned objects are removed.
@export var minimum_position := -10.:
	set(value):
		minimum_position = value


#@export var spawn_density := 1.0
## Milliseconds between spawn events
@export var spawn_time_ms := 100
var last_spawn_time := 0

## Raycast mask to use when placing the drop circle
@export_flags_3d_physics var raycast_collision_mask = 8|2|1 #(int(pow(2,32))-1)^4

## Spawned RigidBodies get assigned this layer
@export_flags_3d_physics var rigidbody_layer = 4
## Spawned RigidBodies get assigned this collision mask
@export_flags_3d_physics var rigidbody_mask = 8|4|2|1



#------------- Editor Actions --------------------

# @export_group("Actions")

# ## This removes all objects that were spawned.
# @export var clear_objects := false:
# 	set(value):
# 		if value && Engine.is_editor_hint() && is_inside_tree():
# 			Clear()

# ## Click to make sure the physics engine is no longer running.
# @export var physics_off := false:
# 	set(value):
# 		if value:
# 			StopPhysics()


#----------------------------------------------------------------------


#var hover_indicator : Decal
#var gizmoplugin : EditorNode3DGizmoPlugin




# Called when the node enters the scene tree for the first time.
func _ready():
	pass
#	if Engine.is_editor_hint():
#		
#		if !has_node("HOVER"):
#			hover_indicator = Decal.new()
#			hover_indicator.name = "HOVER"
#			hover_indicator.size = Vector3(spawn_radius*2, 25, spawn_radius*2)
#			hover_indicator.visible = false
#			
#			var image = Image.load_from_file("res://icon.png")
#			var texture = ImageTexture.create_from_image(image)
#			hover_indicator.texture_albedo = texture
#
#			add_child(hover_indicator)
		
		#ResetHoverBasis()


#func ResetHoverBasis():
#	if hover_indicator == null || !is_inside_tree() || !Engine.is_editor_hint(): return
#	hover_indicator.global_transform.basis = Basis().scaled(10.0 * Vector3.ONE)


#func NodeSelected():
#	print ("Throwout: NodeSelected() ", name)
#	hover_indicator.visible = true
#
#func NodeDeselected():
#	print ("Throwout: NodeDeSelected() ", name)
#	hover_indicator.visible = false



#------ dragging state
var _cached_camera : Camera3D
var mouse_is_down := false
var last_distance := 3.0
var last_raycast_pos := Vector3.ZERO      # world space
var last_raycast_normal := Vector3(0,1,0) # world space


var last_mouse_points : Array
var new_path_points := [] # array of RawPoint, global space
var temp_draw_lines := PackedVector3Array() # local space points

var last_curve_node : Path3D



#var max_cast := 500.0


#--------------------------------------------------------------
#---------------------- Actions -------------------------------
#--------------------------------------------------------------

## Remove only children that are RigidBody3D and have "Spawned" meta.
func Clear():
	for child in get_children():
		#child.queue_free()
		if child is RigidBody3D && child.has_meta("Spawned"):
			child.queue_free()


## Clear all children regardless of what they are.
func ClearAll():
	#print ("ThrowOut Clear all...")
	for child in get_children():
		child.queue_free()


# Make the current spawned objects not be subject to physics any more
func KeepCurrent():
	for child in get_children():
		if child is RigidBody3D && child.has_meta("Spawned"):
			child.set_meta("SpawnedOld", child.get_meta("Spawned")) # maybe we want the set later
			child.remove_meta("Spawned")
			child.freeze = true


var is_simulating := false

func StartPhysics():
	PhysicsServer3D.set_active(true)
	is_simulating = true

func StopPhysics():
	PhysicsServer3D.set_active(false)
	is_simulating = false
	is_spawning = false


#-------------------------- Spawning -------------------------

func _process(delta):
	if is_spawning:
		SpawnTick()
	
	for child in get_children():
		if child.global_position.y < minimum_position:
			child.queue_free()


var is_spawning := false

func SpawnTick():
	if !meshes: return
	
	var mesh_list : Array = meshes.get_item_list()
	
	var cur_time := Time.get_ticks_msec()
	if cur_time - last_spawn_time > spawn_time_ms:
		var num = (cur_time - last_spawn_time) / spawn_time_ms
		if num <= 0: return
		if num > 10: num = 10
		#print ("spawn: ", num)
		for i in range(num):
			var rb = RigidBody3D.new()
			var scene_root = get_tree().edited_scene_root
			var new_owner = scene_root.get_tree().edited_scene_root
			rb.name = "Body"
			rb.set_meta("Spawned", cur_time)
			rb.top_level = true
			var r = spawn_radius * sqrt(randf())
			var theta = randf() * 2 * PI
			var pos := last_raycast_pos + Vector3(r * cos(theta), spawn_height, r * sin(theta))
			rb.collision_layer = rigidbody_layer
			rb.collision_mask = rigidbody_mask
			
			var mesh := MeshInstance3D.new()
			#var mesh_tr := Transform3D
			var col := CollisionShape3D.new()
			#var col_tr := Transform3D
			rb.add_child(mesh)
			rb.add_child(col)
			add_child(rb)
			
			
			if mesh_list.size() > 0:
				var id: int = mesh_list[randi_range(0, mesh_list.size()-1)]
				mesh.mesh = meshes.get_item_mesh(id)
				mesh.transform = meshes.get_item_mesh_transform(id)
				var shapes := meshes.get_item_shapes(id)
				col.shape = shapes[0]
				col.transform = shapes[1]
				rb.name = meshes.get_item_name(id)
				mesh.name = rb.name
				col.name = rb.name+"-col"
			else:
				mesh.mesh = BoxMesh.new()
				col.shape = BoxShape3D.new()
			
			var rb_scale : float = randf()*(spawn_scale.y - spawn_scale.x) + spawn_scale.x
			mesh.scale = Vector3(rb_scale, rb_scale, rb_scale)
			col.scale = Vector3(rb_scale, rb_scale, rb_scale)
			
			rb.owner = new_owner
			col.owner = new_owner
			mesh.owner = new_owner
			
			rb.global_position = pos
			rb.rotation = Vector3(2*PI*randf(), 2*PI*randf(), 2*PI*randf())
		
		
		StartPhysics()
		last_spawn_time = cur_time



func ConvertToStatic():
	StopPhysics()
	
	for child in get_children():
		if not (child is RigidBody3D && child.has_meta("Spawned")): continue
		
		var sb := StaticBody3D.new()
		var trn : Transform3D = child.global_transform
		child.replace_by(sb, true)
		sb.name = child.name
		sb.global_transform = trn
		sb.collision_layer = child.collision_layer
		sb.collision_mask = child.collision_mask
		child.free()


func ConvertToMeshes():
	StopPhysics()
	
	for child in get_children():
		if not (child is RigidBody3D && child.has_meta("Spawned")): continue
		
		var mi : MeshInstance3D
		for ch in child.get_children():
			if ch is MeshInstance3D:
				mi = ch
				break
		if !mi: continue
		
		#print ("Reparenting ", mi.name)
		var old_name = mi.name
		mi.reparent(self, true)
		mi.owner = owner
		mi.name = old_name
		
		child.queue_free()


func ConvertToMultiMeshes():
	StopPhysics()
	
	var pots := {} # dict of array of MeshInstance that use a particular mesh
	var mmis := {} # already existing MultiMeshInstances to add to
	
	for child in get_children():
		if child is MultiMeshInstance3D:
			mmis[child.multimesh.mesh] = child
			#print ("found old mmi: ", child.name, ", mesh: ", child.multimesh.mesh)
			continue
		if not (child is RigidBody3D && child.has_meta("Spawned")): continue
		
		var mi : MeshInstance3D
		for ch in child.get_children():
			if ch is MeshInstance3D:
				mi = ch
				break
		if !mi: continue
		
		if !pots.has(mi.mesh):
			pots[mi.mesh] = [ mi ]
		else:
			pots[mi.mesh].append(mi)
	
	for key in pots:
		#print ("pot: ", key, pots[key][0].name)
		var mmi : MultiMeshInstance3D
		var old_count : int = 0
		if mmis.has(pots[key][0].mesh):
			mmi = mmis[pots[key][0].mesh]
			old_count = mmi.multimesh.instance_count
			mmi.multimesh.instance_count = old_count + pots[key].size()
			mmi.multimesh.visible_instance_count = old_count + pots[key].size()
		else:
			mmi = MultiMeshInstance3D.new()
			mmis[pots[key][0].mesh] = mmi
			mmi.multimesh = MultiMesh.new()
			mmi.multimesh.transform_format = MultiMesh.TRANSFORM_3D
			mmi.multimesh.mesh = pots[key][0].mesh
			mmi.multimesh.instance_count = pots[key].size()
			mmi.multimesh.visible_instance_count = pots[key].size()
			add_child(mmi)
			mmi.owner = owner
		
		var old_name : String = pots[key][0].name
		old_name = old_name.rstrip("0123456789")
		#print ("new name: ", old_name)
		for i in range(pots[key].size()):
			var mi : MeshInstance3D = pots[key][i]
			var trn: Transform3D = global_transform.affine_inverse() * mi.get_parent().transform * mi.transform
			mmi.multimesh.set_instance_transform(old_count + i, trn)
			mi.get_parent().name = "DONEZO" # this prevents reappending numbers to mmi name
			mi.get_parent().queue_free()
		
		mmi.name = old_name


