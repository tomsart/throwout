@tool
extends EditorPlugin

@export var test := 1.0

#---------------------- default settings -------------------------

var default_spawn_radius := 3.0
var max_cast := 500.0
@export_flags_3d_physics var raycast_collision_mask = pow(2,32)-1


#----------------------- misc state ------------------------------------
#const ThrowOutGizmo = preload("res://addons/throwout/throwout_gizmo.gd")
#var throwout_gizmo : EditorNode3DGizmoPlugin = ThrowOutGizmo.new()

var object_toolbar : MenuButton # displayed in toolbar just above viewport

var current_obj : ThrowOut

var hover_indicator : Decal
var mouse_is_down := false
var _cached_camera : Camera3D

enum BrushMode { Spawn, BrushSize }
var brush_mode := BrushMode.Spawn

enum ToolMenu {
	PlopMode,
	SpoutMode,
	ConvertToStatic,
	ConvertToMultiMesh,
	ConvertToMeshes,
	ClearObjects,
	ClearAll,
	KeepCurrent,
	PhysicsOff,
	PhysicsOn
}

var tool_mode := ToolMenu.PlopMode


#----------------------------- Overrides for EditorPlugin ---------------------------------

func get_plugin_name() -> String:
	return 'ThrowOut'


#func _ready():
#	set_input_event_forwarding_always_enabled()


func _enter_tree():
	#print ("ThrowOut plugin _enter_tree")
	#add_custom_types()
	#add_node_3d_gizmo_plugin(throwout_gizmo)
	
	object_toolbar = MenuButton.new()
	var popup := object_toolbar.get_popup()
	object_toolbar.text = "ThrowOut"
	object_toolbar.icon = load("res://addons/throwout/icon.svg")
	popup.name = "ThrowOut"
	popup.clear()
	#popup.add_check_item("Plop mode",  ToolMenu.PlopMode)
	#if tool_mode == ToolMenu.PlopMode: popup.set_item_checked(popup.get_item_index(ToolMenu.PlopMode), true)
	#popup.add_check_item("Spout mode", ToolMenu.SpoutMode)
	#if tool_mode == ToolMenu.SpoutMode: popup.set_item_checked(popup.get_item_index(ToolMenu.SpoutMode), true)
	#popup.add_separator()
	popup.add_item("Convert to StaticBodies",  ToolMenu.ConvertToStatic)
	popup.add_item("Convert to MultiMeshes",   ToolMenu.ConvertToMultiMesh)
	popup.add_item("Convert to MeshInstances", ToolMenu.ConvertToMeshes)
	popup.add_separator()
	popup.add_item("Keep recent",              ToolMenu.KeepCurrent)
	popup.add_item("Clear recent",             ToolMenu.ClearObjects)
	popup.add_item("Clear all",                ToolMenu.ClearAll)
	popup.add_separator()
	popup.add_item("Turn on physics",          ToolMenu.PhysicsOn)
	popup.add_item("Turn off physics",         ToolMenu.PhysicsOff)
	popup.id_pressed.connect(_on_toolbar_menu)
	
	add_control_to_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_MENU, object_toolbar)
	
	# Add visible brush:
	hover_indicator = Decal.new()
	hover_indicator.name = "HOVER"
	hover_indicator.size = Vector3(default_spawn_radius*2, 25, default_spawn_radius*2)
	hover_indicator.visible = false
	#var image = Image.load_from_file("res://addons/throwout/hover_circle.png")
	#var texture = ImageTexture.create_from_image(image)
	var texture = load("res://addons/throwout/hover_circle.png")
	hover_indicator.texture_albedo = texture
	add_child(hover_indicator)
	
	#get_editor_interface().get_selection().selection_changed.connect(selection_changed)


func _exit_tree():
	#print ("ThrowOut plugin _exit_tree")
	
	current_obj = null
	hover_indicator.queue_free()
	hover_indicator = null
	#get_editor_interface().get_selection().selection_changed.disconnect(selection_changed)
	remove_control_from_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_MENU, object_toolbar)
	#remove_node_3d_gizmo_plugin(throwout_gizmo)


# Allows editor to forward us the spatial GUI input for proper objects
func _handles(object):
	return object is ThrowOut

func _edit(object):
	#print ("throwout _edit(",object,")")
	if object: NodeSelected(object)
	else: NodeDeselected(current_obj)
	current_obj = object

func NodeSelected(obj: ThrowOut):
	#print ("Throwout: NodeSelected() ", obj.name)
	hover_indicator.visible = true
	raycast_collision_mask = obj.raycast_collision_mask
	default_spawn_radius = obj.spawn_radius
	hover_indicator.size = Vector3(default_spawn_radius*2, 25, default_spawn_radius*2)

func NodeDeselected(obj: ThrowOut):
	#print ("Throwout: NodeDeSelected() ", obj.name)
	hover_indicator.visible = false


func _make_visible(visible):
	if visible:
		object_toolbar.visible = true
		#print ("throwout make visible: true")
	else:
		object_toolbar.visible = false
		PhysicsServer3D.set_active(false)
		#print ("throwout make visible: false")


func _on_toolbar_menu(id):
	#print("toolbar: ", id)
	match id:
		ToolMenu.PhysicsOff:
			PhysicsServer3D.set_active(false)
		
		ToolMenu.PhysicsOn:
			PhysicsServer3D.set_active(true)
			
		ToolMenu.ConvertToStatic:
			PhysicsServer3D.set_active(false)
			if is_instance_valid(current_obj):
				current_obj.ConvertToStatic()
		
		ToolMenu.ConvertToMultiMesh:
			PhysicsServer3D.set_active(false)
			if is_instance_valid(current_obj):
				current_obj.ConvertToMultiMeshes()
		
		ToolMenu.ConvertToMeshes:
			PhysicsServer3D.set_active(false)
			if is_instance_valid(current_obj):
				current_obj.ConvertToMeshes()
		
		ToolMenu.ClearObjects:
			PhysicsServer3D.set_active(false)
			if is_instance_valid(current_obj):
				current_obj.Clear()
		
		ToolMenu.ClearAll:
			PhysicsServer3D.set_active(false)
			if is_instance_valid(current_obj):
				current_obj.ClearAll()
		
		ToolMenu.KeepCurrent:
			PhysicsServer3D.set_active(false)
			if is_instance_valid(current_obj):
				current_obj.KeepCurrent()
		
		ToolMenu.SpoutMode:
			tool_mode = ToolMenu.SpoutMode
			push_error("IMPLEMENT ME!!!")
		
		ToolMenu.PlopMode:
			tool_mode = ToolMenu.PlopMode
			push_error("IMPLEMENT ME!!!")


#---------------------- input funcs --------------------------------

# Handle events
# Propagate editor camera
# Forward input to ThrowOut if selected
func _forward_3d_gui_input(camera, event):
	var handled = false
	#print ("throwout plugin got event: ", event)
	
	if is_instance_valid(current_obj):
		handled = forwarded_input(camera, event)
	
	return handled



# This is called from EditorPlugin._forward_3d_gui_input() when there is an acceptable node
func forwarded_input(camera, event):
	_cached_camera = camera
	if event is InputEventMouse || event is InputEventMouseMotion || event is InputEventMouseButton:
		#print ("throwout ",current_obj," got input: ", event)
		if event is InputEventMouseButton:
			if event.button_index != MOUSE_BUTTON_LEFT:
				return EditorPlugin.AFTER_GUI_INPUT_PASS
			if event.pressed: # button down
				MouseDown(event)
				return EditorPlugin.AFTER_GUI_INPUT_STOP
			else:
				MouseUp()
				return EditorPlugin.AFTER_GUI_INPUT_STOP
		elif event is InputEventMouseMotion:
			if !event.relative.is_zero_approx():
				if mouse_is_down:
					MouseDrag(event)
					return EditorPlugin.AFTER_GUI_INPUT_STOP
				else:
					MouseMove()
			return EditorPlugin.AFTER_GUI_INPUT_PASS
		return EditorPlugin.AFTER_GUI_INPUT_STOP
	elif event is InputEventKey:
		if event.physical_keycode == KEY_SHIFT || event.physical_keycode == KEY_CTRL:
			if event.physical_keycode == KEY_CTRL:
				if event.pressed:
					hover_indicator.modulate = Color(1.0, 1.0, 0.0)
					brush_mode = BrushMode.BrushSize
				else:
					hover_indicator.modulate = Color(1.0, 1.0, 1.0)
					brush_mode = BrushMode.Spawn
	return EditorPlugin.AFTER_GUI_INPUT_PASS


func MouseDown(event):
	#print("ThrowOut mouse down")
	mouse_is_down = true
	if is_instance_valid(current_obj):
		if brush_mode == BrushMode.Spawn:
			current_obj.is_spawning = true
			current_obj.SpawnTick()


func MouseUp():
	#print("ThrowOut mouse up")
	mouse_is_down = false
	if brush_mode == BrushMode.BrushSize && !Input.is_key_pressed(KEY_CTRL):
		brush_mode = BrushMode.Spawn
		hover_indicator.modulate = Color(1.0, 1.0, 1.0)
	if is_instance_valid(current_obj):
		current_obj.is_spawning = false


func MouseDrag(event: InputEventMouseMotion):
	#print("Throwout mouse drag")
	update_hover_indicator()
	if brush_mode == BrushMode.BrushSize:
		var diff : float = event.relative.x
		var factor : float = 1.0
		if diff > 0: factor = 1.0 + diff / 50
		elif diff < 0: factor = 1 / (1.0 - diff / 50)
		default_spawn_radius *= factor
		current_obj.spawn_radius = default_spawn_radius
		#print (factor)
		hover_indicator.size = Vector3(default_spawn_radius*2, 25, default_spawn_radius*2)


func MouseMove():
	update_hover_indicator()


# Update brush decal.
func update_hover_indicator():
	if !current_obj: return
	
	if current_obj.spawn_radius != default_spawn_radius:
		default_spawn_radius = current_obj.spawn_radius
		hover_indicator.size = Vector3(default_spawn_radius*2, 25, default_spawn_radius*2)
	
	var world := current_obj.get_world_3d()
	if !world:
		current_obj.is_simulating = false
		PhysicsServer3D.set_active(false)
		return
	
	var space_state := world.direct_space_state
	var start := _cached_camera.global_position
	var end   := project_mouse(max_cast)
	var params := PhysicsRayQueryParameters3D.new()
	params.from = start
	params.to = end
	params.collision_mask = current_obj.raycast_collision_mask
	var ray_result : Dictionary = space_state.intersect_ray(params)
	
	var hit_pos : Vector3 #= last_raycast_pos
	var hit_normal : Vector3 #= last_raycast_normal
	
	if !ray_result.is_empty():
		hit_pos = ray_result.position
		hit_normal = ray_result.normal
		hover_indicator.global_position = hit_pos + Vector3(0,5,0)
		current_obj.last_raycast_pos = hit_pos


#--------------------------------------------------------------------
#---------------------- Utility funcs -------------------------------
#--------------------------------------------------------------------

# Project mouse position onto a plane that is distance away from the camera origin at its closest point.
# Note this is NOT the final distance between camera and projected point.
func project_mouse(distance: float, offset: Vector2 = Vector2.ZERO) -> Vector3:
	return _cached_camera.project_position(_cached_camera.get_viewport().get_mouse_position() + offset, distance)

